package com.nnk.springboot;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.repositories.ICurvePointRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurvePointIT {

    CurvePoint curvePoint;
    @Autowired
    private ICurvePointRepository iCurvePointRepository;

    @Before
    public void setUp() throws Exception {
        curvePoint = new CurvePoint();
        curvePoint.setValue(182.69);
        curvePoint.setTerm(251.55);
        curvePoint.setCurveId(93);
        curvePoint.setId(100);
    }

    @Test
    public void deleteCurvePointTest_shouldDeleteTheCurvePointFromDatabase_forAnExistingCurvePoint() {
        System.out.println(curvePoint);
        var savedCurvePoint = iCurvePointRepository.save(curvePoint);
        Integer id = savedCurvePoint.getId();
        iCurvePointRepository.delete(savedCurvePoint);
        Optional<CurvePoint> deletedCurvePoint = iCurvePointRepository.findById(id);
        System.out.println(deletedCurvePoint);
        Assert.assertFalse(deletedCurvePoint.isPresent());
    }

    @Test
    public void findCurvePointTest_shouldGetAllTheCurvePointsFromDatabase() {
        List<CurvePoint> listResult = iCurvePointRepository.findAll();
        Assert.assertFalse(listResult.size() <= 0);
    }

    @Test
    public void updateTest() {
        curvePoint.setCurveId(20);
        curvePoint = iCurvePointRepository.save(curvePoint);
        Assert.assertEquals(20, (int) curvePoint.getCurveId());
    }

    @Test
    public void saveTest() {
        curvePoint = iCurvePointRepository.save(curvePoint);
        Assert.assertNotNull(curvePoint.getId());
        Assert.assertEquals(93, (int) curvePoint.getCurveId());
    }

}