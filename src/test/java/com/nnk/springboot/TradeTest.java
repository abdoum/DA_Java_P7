package com.nnk.springboot;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.ITradeRepository;
import com.nnk.springboot.service.TradeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {TradeServiceImpl.class})
@ExtendWith(SpringExtension.class)
class TradeTest {

    @MockBean
    ITradeRepository iTradeRepository;

    @Autowired
    private TradeServiceImpl tradeService;

    private Trade trade;

    @BeforeEach
    void setUp() {
        trade = new Trade();
        trade.setId(12);
        trade.setType("boil");
        trade.setAccount("w32Kz2");
        trade.setCreationDate(Timestamp.from(Instant.now()));
        trade.setBenchmark("bedroom");
        trade.setBook("prison");
        trade.setTrader("whip");
        trade.setBuyPrice(234.09);
        trade.setBuyQuantity(914D);
        trade.setCreationName("KGEE");
        trade.setDealName("electric");
        trade.setDealType("anger");
        trade.setRevisionDate(Timestamp.from(Instant.now().plus(12, DAYS)));
        trade.setSecurity("fCbvbCMv");
        trade.setSide("l52DZ");
        trade.setSellPrice(483.41);
        trade.setSellQuantity(918.70);
        trade.setStatus("take");
        trade.setSourceListId("3B5ekB72");
    }

    @Test
    void saveTest_shouldReturnTheSavedTrade_forAValidTrade() {
        when(this.iTradeRepository.save((Trade) any())).thenReturn(trade);
        assertSame(trade, this.tradeService.save(trade));
        verify(this.iTradeRepository).save((Trade) any());
    }

    @Test
    void updateTest_shouldReturnTheSavedTrade_forAValidTrade() throws BadArgument {
        trade.setSellPrice(700D);
        when(this.iTradeRepository.getById((Integer) any())).thenReturn(trade);
        when(this.iTradeRepository.save((Trade) any())).thenReturn(trade);
        var actualResult = this.tradeService.update(trade);
        assertSame(trade, actualResult);
        assertEquals(700D, actualResult.getSellPrice());
        verify(this.iTradeRepository).getById((Integer) any());
        verify(this.iTradeRepository).save((Trade) any());
    }

    @Test
    void updateTest_shouldThrowBadArgumentException_forANonExistingTrade() throws BadArgument {
        when(this.iTradeRepository.getById((Integer) any())).thenReturn(null);
        assertThrows(BadArgument.class, () -> this.tradeService.update(trade));
        verify(this.iTradeRepository).getById((Integer) any());
        verify(this.iTradeRepository, never()).save((Trade) any());
    }

    @Test
    void findAllTest_shouldReturnTheSavedTrade_forAValidTrade() {
        List<Trade> tradeList = new ArrayList<Trade>();
        tradeList.add(trade);
        when(this.iTradeRepository.findAll()).thenReturn(tradeList);
        assertSame(tradeList, this.tradeService.findAll());
        verify(iTradeRepository).findAll();
    }

    @Test
    void getByIdTest_shouldReturnTheSavedTrade_forAValidTrade() {
        when(this.iTradeRepository.getById((Integer) any())).thenReturn(trade);
        assertSame(trade, this.tradeService.getById(12));
        verify(this.iTradeRepository).getById(12);
    }

}
