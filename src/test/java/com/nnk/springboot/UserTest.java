package com.nnk.springboot;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.IUserRepository;
import com.nnk.springboot.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {UserServiceImpl.class})
@ExtendWith(SpringExtension.class)
class UserTest {

    @MockBean
    IUserRepository iUserRepository;

    @MockBean
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserServiceImpl userService;

    private User user;

    @BeforeEach
    void setUp() {
        user = new User();
        user.setId(12);
        user.setRole("USER");
        user.setUsername("doubt");
        user.setPassword("acheE3#s23");
        user.setFullname("evening besides");
    }

    @Test
    void saveTest_shouldReturnTheSavedUser_forAValidUser() {
        when(this.iUserRepository.save((User) any())).thenReturn(user);
        when(this.bCryptPasswordEncoder.encode((String) any())).thenReturn("v3xmlUVN");
        assertSame(user, this.userService.save(user));
        verify(this.iUserRepository).save((User) any());
        verify(this.bCryptPasswordEncoder).encode((String) any());
    }

    @Test
    void updateTest_shouldReturnTheSavedUser_forAValidUser() throws BadArgument {
        user.setFullname("verse now");
        when(this.iUserRepository.getById((Integer) any())).thenReturn(user);
        when(this.bCryptPasswordEncoder.encode((String) any())).thenReturn("v3xmlUVN");
        when(this.iUserRepository.save((User) any())).thenReturn(user);
        var actualResult = this.userService.update(user);
        assertSame(user, actualResult);
        assertEquals("Verse Now", actualResult.getFullname());
        verify(this.iUserRepository).getById((Integer) any());
        verify(this.iUserRepository).save((User) any());
        verify(this.bCryptPasswordEncoder).encode((String) any());
    }

    @Test
    void updateTest_shouldThrowBadArgumentException_forANonExistingUser() throws BadArgument {
        when(this.iUserRepository.getById((Integer) any())).thenReturn(null);
        assertThrows(BadArgument.class, () -> this.userService.update(user));
        verify(this.iUserRepository).getById((Integer) any());
        verify(this.iUserRepository, never()).save((User) any());
    }

    @Test
    void findAllTest_shouldReturnTheSavedUser_forAValidUser() {
        List<User> userList = new ArrayList<User>();
        userList.add(user);
        when(this.iUserRepository.findAll()).thenReturn(userList);
        assertSame(userList, this.userService.findAll());
        verify(iUserRepository).findAll();
    }

    @Test
    void getByIdTest_shouldReturnTheSavedUser_forAValidUser() {
        when(this.iUserRepository.getById((Integer) any())).thenReturn(user);
        assertSame(user, this.userService.getById(12));
        verify(this.iUserRepository).getById(12);
    }
}
