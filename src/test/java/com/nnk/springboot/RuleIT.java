package com.nnk.springboot;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.repositories.IRuleNameRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kopitubruk.util.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RuleIT {

    RuleName rule;
    @Autowired
    private IRuleNameRepository IRuleNameRepository;

    @Before
    public void setUp() throws Exception {
        Map<String, Object> myData = new HashMap<>();
        myData.put("huks", "bied");
        myData.put("a", "s");
        String jsonStr = JSONUtil.toJSON(myData);
        rule = new RuleName("Rule Name", "Description",
                jsonStr,
                "Template", "SQL", "SQL Part");
        rule.setName("Rule Name");
        rule.setDescription("Description");
        rule.setJson("Template");
        rule.setSqlStr("SQL");
        rule.setSqlPart("SQL Part");
    }

    @Test
    public void saveRuleTest_shouldSaveARuleToDatabase_forAValidRule() {
        rule = IRuleNameRepository.save(rule);
        Assert.assertNotNull(rule.getId());
        Assert.assertEquals("Rule Name", rule.getName());
    }

    @Test
    public void deleteRuleTest_shouldDeleteARuleFromDatabase_forAValidRule() {
        var savedRule = IRuleNameRepository.save(rule);
        Integer id = savedRule.getId();
        IRuleNameRepository.delete(savedRule);
        Optional<RuleName> deletedRule = IRuleNameRepository.findById(id);
        Assert.assertFalse(deletedRule.isPresent());
    }

    @Test
    public void findRuleTest_shouldGetAllRulesFromDatabase() {
        List<RuleName> listResult = IRuleNameRepository.findAll();
        Assert.assertTrue(listResult.size() > 0);
    }

    @Test
    public void updateRuleTest_shouldUpdateARuleInDatabase_forAnExistingValidRule() {
        rule.setName("Rule Name Update");
        rule = IRuleNameRepository.save(rule);
        Assert.assertEquals("Rule Name Update", rule.getName());
    }
}