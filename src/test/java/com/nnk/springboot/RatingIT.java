package com.nnk.springboot;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.repositories.IRatingRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RatingIT {

    Rating rating;
    @Autowired
    private IRatingRepository IRatingRepository;

    @Before
    public void setUp() throws Exception {
        rating = new Rating("Moodys Rating", "Sand PRating", "Fitch Rating", 10);
    }

    @Test
    public void saveRatingTest_shouldSaveARatingToDatabase_forAValidRating() {
        var savedRating = IRatingRepository.save(rating);
        Assert.assertNotNull(savedRating.getId());
        Assert.assertEquals(10, (int) savedRating.getOrderNumber());
    }

    @Test
    public void updateRatingTest_shouldUpdateARatingToDatabase_forAValidRating() {
        rating.setOrderNumber(20);
        var updatedRating = IRatingRepository.save(rating);
        Assert.assertEquals(20, (int) updatedRating.getOrderNumber());
    }

    @Test
    public void findRatingTest_shouldGetAllARatingsFromDatabase_forAValidRating() {
        List<Rating> listResult = IRatingRepository.findAll();
        Assert.assertTrue(listResult.size() > 0);
    }

    @Test
    public void deleteRatingTest_shouldDeleteARatingFromDatabase_forAnExistingValidRating() {
        var savedRating = IRatingRepository.save(rating);
        Integer id = rating.getId();
        IRatingRepository.delete(savedRating);
        Optional<Rating> deletedRating = IRatingRepository.findById(id);
        Assert.assertFalse(deletedRating.isPresent());
    }
}