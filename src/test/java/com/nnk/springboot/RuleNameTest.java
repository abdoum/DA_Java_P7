package com.nnk.springboot;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.IRuleNameRepository;
import com.nnk.springboot.service.RuleNameServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kopitubruk.util.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {RuleNameServiceImpl.class})
@ExtendWith(SpringExtension.class)
class RuleNameTest {

    @MockBean
    IRuleNameRepository iRuleNameRepository;

    @Autowired
    private RuleNameServiceImpl ruleNameService;

    private RuleName ruleName;

    @BeforeEach
    void setUp() {
        Map<String, Object> myData = new HashMap<>();
        myData.put("huks", "bied");
        myData.put("a", "s");
        String jsonStr = JSONUtil.toJSON(myData);
        ruleName = new RuleName("Rule Name", "Description",
                jsonStr,
                "Template", "SQL", "SQL Part");
        ruleName.setId(12);
    }

    @Test
    void saveRuleNameTest_shouldReturnTheSavedRuleName_forAValidRuleName() {
        when(this.iRuleNameRepository.save((RuleName) any())).thenReturn(ruleName);
        assertSame(ruleName, this.ruleNameService.save(ruleName));
        verify(this.iRuleNameRepository).save((RuleName) any());
    }

    @Test
    void updateRuleNameTest_shouldReturnTheSavedRuleName_forAValidRuleName() throws BadArgument {
        ruleName.setName("modified name");
        when(this.iRuleNameRepository.getById((Integer) any())).thenReturn(ruleName);
        when(this.iRuleNameRepository.save((RuleName) any())).thenReturn(ruleName);
        var actualResult = this.ruleNameService.update(ruleName);
        assertSame(ruleName, actualResult);
        assertEquals(12, actualResult.getId());
        verify(this.iRuleNameRepository).getById((Integer) any());
        verify(this.iRuleNameRepository).save((RuleName) any());
    }

    @Test
    void updateRuleNameTest_shouldThrowBadArgumentException_forANonExistingRuleName() throws BadArgument {
        when(this.iRuleNameRepository.getById((Integer) any())).thenReturn(null);
        assertThrows(BadArgument.class, () -> this.ruleNameService.update(ruleName));
        verify(this.iRuleNameRepository).getById((Integer) any());
        verify(this.iRuleNameRepository, never()).save((RuleName) any());
    }

    @Test
    void findAllRuleNameTest_shouldReturnTheSavedRuleName_forAValidRuleName() {
        List<RuleName> ruleNameList = new ArrayList<RuleName>();
        ruleNameList.add(ruleName);
        when(this.iRuleNameRepository.findAll()).thenReturn(ruleNameList);
        assertSame(ruleNameList, this.ruleNameService.findAll());
        verify(iRuleNameRepository).findAll();
    }

    @Test
    void getRuleNameByIdTest_shouldReturnTheSavedRuleName_forAValidRuleName() {
        when(this.iRuleNameRepository.getById((Integer) any())).thenReturn(ruleName);
        assertSame(ruleName, this.ruleNameService.getById(12));
        verify(this.iRuleNameRepository).getById(12);
    }
}