package com.nnk.springboot;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.IBidListRepository;
import com.nnk.springboot.service.BidListServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {BidListServiceImpl.class})
@ExtendWith(SpringExtension.class)
class BidListTest {

    @MockBean
    IBidListRepository iBidListRepository;

    @Autowired
    private BidListServiceImpl bidListService;

    private BidList bidList;

    @BeforeEach
    void setUp() {
        bidList = new BidList();
        bidList.setBidQuantity(225.82);
        bidList.setType("town");
        bidList.setAccount("9px5Fk3r");
        bidList.setCreationDate(Timestamp.from(Instant.now()));
    }

    @Test
    void saveTest_shouldReturnTheSavedBidList_forAValidBidList() {
        when(this.iBidListRepository.save((BidList) any())).thenReturn(bidList);
        List<BidList> bidListList = new ArrayList<BidList>();
        bidListList.add(bidList);
        when(this.iBidListRepository.findAll()).thenReturn(bidListList);
        assertSame(bidList, this.bidListService.save(bidList).get(0));
        verify(this.iBidListRepository).save((BidList) any());
    }

    @Test
    void updateTest_shouldReturnTheSavedBidList_forAValidBidList() throws BadArgument {
        bidList.setBidQuantity(7D);
        when(this.iBidListRepository.getById((Integer) any())).thenReturn(bidList);
        when(this.iBidListRepository.save((BidList) any())).thenReturn(bidList);
        List<BidList> bidListList = new ArrayList<BidList>();
        bidListList.add(bidList);
        when(this.iBidListRepository.findAll()).thenReturn(bidListList);
        var actualResult = this.bidListService.update(bidList);
        assertSame(bidList, actualResult.get(0));
        assertEquals(7D, actualResult.get(0).getBidQuantity());
        verify(this.iBidListRepository).getById((Integer) any());
        verify(this.iBidListRepository).save((BidList) any());
    }

    @Test
    void updateTest_shouldThrowBadArgumentException_forANonExistingBidList() throws BadArgument {
        when(this.iBidListRepository.getById((Integer) any())).thenReturn(null);
        assertThrows(BadArgument.class, () -> this.bidListService.update(bidList));
        verify(this.iBidListRepository).getById((Integer) any());
        verify(this.iBidListRepository, never()).save((BidList) any());
    }

    @Test
    void findAllTest_shouldReturnTheSavedBidList_forAValidBidList() {
        List<BidList> bidListList = new ArrayList<BidList>();
        bidListList.add(bidList);
        when(this.iBidListRepository.findAll()).thenReturn(bidListList);
        assertSame(bidListList, this.bidListService.findAll());
        verify(iBidListRepository).findAll();
    }

    @Test
    void getByIdTest_shouldReturnTheSavedBidList_forAValidBidList() {
        when(this.iBidListRepository.getById((Integer) any())).thenReturn(bidList);
        assertSame(bidList, this.bidListService.getById(12));
        verify(this.iBidListRepository).getById(12);
    }
}
