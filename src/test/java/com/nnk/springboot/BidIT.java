package com.nnk.springboot;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.repositories.IBidListRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BidIT {

    BidList bid;
    @Autowired
    private IBidListRepository IBidListRepository;

    @Before
    public void setUp() throws Exception {
        bid = new BidList("Account Test", "Type Test", 10d);
        bid.setAccount("Account Test");
        bid.setType("Type Test");
        bid.setBid(10D);
        bid.setBidQuantity(10d);
    }

    @Test
    @DisplayName("Delete a bid from db. Should delete the bid for an existing bid")
    public void saveBidListBidListTest_shouldSaveTheBidListToTheDataBase_forAValidBidList() {
        var savedBid = IBidListRepository.save(bid);
        Assert.assertNotNull(savedBid);
        Assert.assertEquals(10d, savedBid.getBidQuantity(), 10d);
    }

    @Test
    public void findBidListTest_shouldReturnTheBidListFromTheDataBase() {
        List<BidList> listResult = IBidListRepository.findAll();
        Assert.assertTrue(listResult.size() > 0);
    }

    @Test
    public void updateBidListTest_shouldUpdateTheBidListToTheDataBase_forAValidBidList() {
        bid.setBidQuantity(20d);
        var updatedBid = IBidListRepository.save(bid);
        Assert.assertEquals(20d, updatedBid.getBidQuantity(), 20d);
    }

    @Test
    public void deleteBidListTest_shouldDeleteTheBidListFromTheDataBase_forAValidBidList() {
        var savedBid = IBidListRepository.save(bid);
        Integer id = savedBid.getBidListId();
        IBidListRepository.delete(bid);
        Optional<BidList> bidList = IBidListRepository.findById(id);
        Assert.assertFalse(bidList.isPresent());
    }
}