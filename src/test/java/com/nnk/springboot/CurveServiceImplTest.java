package com.nnk.springboot;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.ICurvePointRepository;
import com.nnk.springboot.service.CurvePointServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {CurvePointServiceImpl.class})
@ExtendWith(SpringExtension.class)
class CurveServiceImplTest {

    @MockBean
    ICurvePointRepository iCurvePointRepository;

    @Autowired
    private CurvePointServiceImpl curvePointService;

    private CurvePoint curvePoint;

    @BeforeEach
    void setUp() {
        curvePoint = new CurvePoint();
        curvePoint.setId(12);
        curvePoint.setTerm(56D);
        curvePoint.setAsOfDate(Timestamp.valueOf("2021-08-02 01:03:23"));
        curvePoint.setValue(87D);
        curvePoint.setCreationDate(Timestamp.from(Instant.now()));
    }

    @Test
    void saveTest_shouldReturnTheSavedCurvePoint_forAValidCurvePoint() {
        when(this.iCurvePointRepository.save((CurvePoint) any())).thenReturn(curvePoint);
        assertSame(curvePoint, this.curvePointService.save(curvePoint));
        verify(this.iCurvePointRepository).save((CurvePoint) any());
    }

    @Test
    void updateTest_shouldReturnTheSavedCurvePoint_forAValidCurvePoint() throws BadArgument {
        curvePoint.setTerm(7D);
        when(this.iCurvePointRepository.getById((Integer) any())).thenReturn(curvePoint);
        when(this.iCurvePointRepository.save((CurvePoint) any())).thenReturn(curvePoint);
        var actualResult = this.curvePointService.update(curvePoint);
        assertSame(curvePoint, actualResult);
        assertEquals(12, actualResult.getId());
        verify(this.iCurvePointRepository).getById((Integer) any());
        verify(this.iCurvePointRepository).save((CurvePoint) any());
    }

    @Test
    void updateTest_shouldThrowBadArgumentException_forANonExistingCurvePoint() throws BadArgument {
        when(this.iCurvePointRepository.getById((Integer) any())).thenReturn(null);
        assertThrows(BadArgument.class, () -> this.curvePointService.update(curvePoint));
        verify(this.iCurvePointRepository).getById((Integer) any());
        verify(this.iCurvePointRepository, never()).save((CurvePoint) any());
    }

    @Test
    void findAllTest_shouldReturnTheSavedCurvePoint_forAValidCurvePoint() {
        List<CurvePoint> curvePointList = new ArrayList<CurvePoint>();
        curvePointList.add(curvePoint);
        when(this.iCurvePointRepository.findAll()).thenReturn(curvePointList);
        assertSame(curvePointList, this.curvePointService.findAll());
        verify(iCurvePointRepository).findAll();
    }

    @Test
    void getByIdTest_shouldReturnTheSavedCurvePoint_forAValidCurvePoint() {
        when(this.iCurvePointRepository.getById((Integer) any())).thenReturn(curvePoint);
        assertSame(curvePoint, this.curvePointService.getById(12));
        verify(this.iCurvePointRepository).getById(12);
    }

}
