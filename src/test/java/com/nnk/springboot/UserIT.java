package com.nnk.springboot;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.repositories.IUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserIT {

    User user;
    @Autowired
    private IUserRepository iUserRepository;

    @BeforeEach
    public void setUp() throws Exception {
        user = new User();
        user.setFullname("slavery nest");
        user.setUsername("slaverynest");
        user.setRole("USER");
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode("roa87.deSd"));
    }

    @Test
    void saveUserTest_shouldSaveTheUserToDatabase_forAValidUser() {
        User savedUser = iUserRepository.save(user);
        assertNotNull(savedUser.getId());
        assertEquals("slaverynest", savedUser.getUsername());
    }


    @Test
    void findUserTest_shouldGetTheUsersFromDatabase() {
        List<User> listResult = iUserRepository.findAll();
        assertTrue(listResult.size() > 0);
    }

    @Test
    void updateUserTest_shouldUpdateTheUserInDatabase_forAValidUser() {
        user.setRole("ADMIN");
        user = iUserRepository.save(user);
        assertEquals("ADMIN", user.getRole());
    }

    @Test
    void deleteUserTest_shouldDeleteTheUserFromDatabase_forAValidUserId() {
        var savedUser = iUserRepository.save(user);
        Integer id = savedUser.getId();
        iUserRepository.delete(savedUser);
        Optional<User> ratingList = iUserRepository.findById(id);
        assertFalse(ratingList.isPresent());
    }
}