package com.nnk.springboot;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.repositories.ITradeRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeIT {

    Trade trade;
    @Autowired
    private ITradeRepository iTradeRepository;

    @Before
    public void setUp() throws Exception {
        trade = new Trade("Trade Account", "Type");
        trade.setType("Type");
        trade.setAccount("Trade Account");
        trade.setBuyPrice(48.63);
        trade.setSellPrice(548.63);
    }

    @Test
    public void saveTradeTest_shouldSaveATradeToDatabase_forAValidTrade() {
        var savedTrade = iTradeRepository.save(trade);
        Assert.assertNotNull(savedTrade.getId());
        Assert.assertEquals("Trade Account", savedTrade.getAccount());
    }

    @Test
    public void deleteTradeTest_shouldDeleteATradeFromDatabase_forAValidTrade() {
        var savedTrade = iTradeRepository.save(trade);
        Integer id = savedTrade.getId();
        iTradeRepository.delete(savedTrade);
        Optional<Trade> tradeList = iTradeRepository.findById(id);
        Assert.assertFalse(tradeList.isPresent());
    }

    @Test
    public void findTradeTest_shouldGetAllTradeFromDatabase() {
        List<Trade> listResult = iTradeRepository.findAll();
        Assert.assertFalse(listResult.size() <= 0);
    }

    @Test
    public void updateTradeTest_shouldUpdateATradeInDatabase_forAnExistingTrade() {
        trade.setAccount("Trade Account Update");
        var updatedTrade = iTradeRepository.save(trade);
        Assert.assertEquals("Trade Account Update", updatedTrade.getAccount());
    }
}