package com.nnk.springboot;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.IRatingRepository;
import com.nnk.springboot.service.RatingServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {RatingServiceImpl.class})
@ExtendWith(SpringExtension.class)
class RatingTest {

    @MockBean
    IRatingRepository iRatingRepository;

    @Autowired
    private RatingServiceImpl ratingService;

    private Rating rating;

    @BeforeEach
    void setUp() {
        rating = new Rating("watch", "recent", "scale", 12);
    }

    @Test
    void saveTest_shouldReturnTheSavedRating_forAValidRating() {
        when(this.iRatingRepository.save((Rating) any())).thenReturn(rating);
        assertSame(rating, this.ratingService.save(rating));
        verify(this.iRatingRepository).save((Rating) any());
    }

    @Test
    void updateTest_shouldReturnTheSavedRating_forAValidRating() throws BadArgument {
        rating.setOrderNumber(7);
        when(this.iRatingRepository.getById((Integer) any())).thenReturn(rating);
        when(this.iRatingRepository.save((Rating) any())).thenReturn(rating);
        var actualResult = this.ratingService.update(rating);
        assertSame(rating, actualResult);
        assertEquals(7, actualResult.getOrderNumber());
        verify(this.iRatingRepository).getById((Integer) any());
        verify(this.iRatingRepository).save((Rating) any());
    }

    @Test
    void updateTest_shouldThrowBadArgumentException_forANonExistingRating() throws BadArgument {
        when(this.iRatingRepository.getById((Integer) any())).thenReturn(null);
        assertThrows(BadArgument.class, () -> this.ratingService.update(rating));
        verify(this.iRatingRepository).getById((Integer) any());
        verify(this.iRatingRepository, never()).save((Rating) any());
    }

    @Test
    void findAllTest_shouldReturnTheSavedRating_forAValidRating() {
        List<Rating> ratingList = new ArrayList<Rating>();
        ratingList.add(rating);
        when(this.iRatingRepository.findAll()).thenReturn(ratingList);
        assertSame(ratingList, this.ratingService.findAll());
        verify(iRatingRepository).findAll();
    }

    @Test
    void getByIdTest_shouldReturnTheSavedRating_forAValidRating() {
        when(this.iRatingRepository.getById((Integer) any())).thenReturn(rating);
        assertSame(rating, this.ratingService.getById(12));
        verify(this.iRatingRepository).getById(12);
    }
}
