package com.nnk.springboot.repositories;

import com.nnk.springboot.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface IUserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {
	User getByUsername(String username);

}
