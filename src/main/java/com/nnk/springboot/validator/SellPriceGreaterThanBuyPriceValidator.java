package com.nnk.springboot.validator;

import com.nnk.springboot.annotation.SellPriceGreaterThanBuyPrice;
import com.nnk.springboot.domain.Trade;
import lombok.Generated;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;

@Generated
@SupportedValidationTarget(ValidationTarget.ANNOTATED_ELEMENT)
public class SellPriceGreaterThanBuyPriceValidator implements
        ConstraintValidator<SellPriceGreaterThanBuyPrice, Trade> {

    @Override
    public void initialize(SellPriceGreaterThanBuyPrice constraintAnnotation) {
    }

    @Override
    public boolean isValid(Trade trade, ConstraintValidatorContext context) {
        if (trade == null || trade.getBuyPrice() == null || trade.getSellPrice() == null) {
            return true;
        }
        return trade.getBuyPrice() <= trade.getSellPrice();
    }
}