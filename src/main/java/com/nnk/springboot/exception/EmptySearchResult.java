package com.nnk.springboot.exception;

import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@Generated
@ResponseStatus(HttpStatus.NOT_FOUND)
public class EmptySearchResult extends Exception {

    public EmptySearchResult(String message) {
        super(message);
        log.error(message);
    }
}
