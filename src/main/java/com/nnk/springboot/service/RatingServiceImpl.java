package com.nnk.springboot.service;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.IRatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingServiceImpl implements IRatingService {

    @Autowired
    IRatingRepository iRatingRepository;

    /**
     * Find all rating in database.
     *
     * @return the list found ratings or an empty list if none found
     */
    @Override
    public List<Rating> findAll() {
        return iRatingRepository.findAll();
    }

    /**
     * Saves a rating to database.
     *
     * @param rating the rating to be saved
     * @return the saved rating
     */
    @Override
    public Rating save(Rating rating) {
        var ratingToPersist = new Rating(rating.getMoodysRating(),
                rating.getSandPRating(),
                rating.getFitchRating(),
                rating.getOrderNumber());
        return iRatingRepository.save(ratingToPersist);
    }

    /**
     * Delete a rating by its id.
     *
     * @param id the id
     */
    @Override
    public void deleteById(Integer id) {
        iRatingRepository.deleteById(id);
    }

    /**
     * Gets a rating by id.
     *
     * @param id the rating id
     * @return the found rating
     */
    @Override
    public Rating getById(Integer id) {
        return iRatingRepository.getById(id);
    }

    /**
     * Updates a rating.
     *
     * @param rating the rating
     * @return the rating
     * @throws BadArgument if the rating with the given id doesn't exist
     */
    @Override
    public Rating update(Rating rating) throws BadArgument {
        var existingRating = getById(rating.getId());
        if (existingRating == null) {
            throw new BadArgument("rating with id : " + rating.getId() + " not found");
        }
        existingRating.setId(rating.getId());
        existingRating.setFitchRating(rating.getFitchRating());
        existingRating.setSandPRating(rating.getSandPRating());
        existingRating.setMoodysRating(rating.getMoodysRating());
        return iRatingRepository.save(existingRating);
    }
}
