package com.nnk.springboot.service;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.IBidListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BidListServiceImpl implements IBidListService {

    @Autowired
    IBidListRepository iBidListRepository;

    /**
     * Find all Bids.
     *
     * @return the Bids
     */
    @Override
    public List<BidList> findAll() {
        return iBidListRepository.findAll();
    }

    /**
     * Saves a bid.
     *
     * @param bidList the bid to be saved
     * @return the list
     */
    @Override
    public List<BidList> save(BidList bidList) {
        var bidToBeSaved = new BidList();
        bidToBeSaved.setBidListId(bidList.getBidListId());
        bidToBeSaved.setAccount(bidList.getAccount());
        bidToBeSaved.setType(bidList.getType());
        bidToBeSaved.setBidQuantity(bidList.getBidQuantity());
        iBidListRepository.save(bidToBeSaved);
        return findAll();
    }

    /**
     * Deletes a bid by id.
     *
     * @param id the id of the bid to be deleted
     */
    @Override
    public void deleteById(Integer id) {
        iBidListRepository.deleteById(id);
    }

    /**
     * Gets a bid by id.
     *
     * @param id the id of the bid to be found
     * @return the found bid
     */
    @Override
    public BidList getById(Integer id) {
        return iBidListRepository.getById(id);
    }

    /**
     * Update an existing bid.
     *
     * @param bidList the bid to be updated
     * @return the list of bids
     * @throws BadArgument the if the bid is not found
     */
    @Override
    public List<BidList> update(BidList bidList) throws BadArgument {
        var existingBidList = getById(bidList.getBidListId());
        if (existingBidList == null) {
            throw new BadArgument("CurvePoint with id : " + bidList.getBidListId() + " not found");
        }
        existingBidList.setBidListId(bidList.getBidListId());
        existingBidList.setAccount(bidList.getAccount());
        existingBidList.setType(bidList.getType());
        existingBidList.setBidQuantity(bidList.getBidQuantity());
        existingBidList.setCreationDate(bidList.getCreationDate());
        iBidListRepository.save(existingBidList);
        return findAll();
    }

}
