package com.nnk.springboot.service;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.exception.BadArgument;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    List<User> findAll();

    User save(User user);

    void deleteById(Integer id);

    User getById(Integer id);

    User update(User user) throws BadArgument;

    Optional<User> findById(Integer id) throws IllegalArgumentException;

    void delete(Integer userId);
}
