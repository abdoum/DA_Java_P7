package com.nnk.springboot.service;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.IRuleNameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RuleNameServiceImpl implements IRuleNameService {

    @Autowired
    IRuleNameRepository iRuleNameRepository;

    /**
     * Find all RuleName in database.
     *
     * @return the list found RuleNames or an empty list if none found
     */
    @Override
    public List<RuleName> findAll() {
        return iRuleNameRepository.findAll();
    }

    /**
     * Saves a RuleName to database.
     *
     * @param ruleName the RuleName to be saved
     * @return the saved RuleName
     */
    @Override
    public RuleName save(RuleName ruleName) {
        var ruleNameToPersist = new RuleName();
        ruleNameToPersist.setName(ruleName.getName());
        ruleNameToPersist.setDescription(ruleName.getDescription());
        ruleNameToPersist.setJson(ruleName.getJson());
        ruleNameToPersist.setSqlPart(ruleName.getSqlPart());
        ruleNameToPersist.setSqlStr(ruleName.getSqlStr());
        ruleNameToPersist.setTemplate(ruleName.getTemplate());

        return iRuleNameRepository.save(ruleNameToPersist);
    }


    /**
     * Delete a RuleName by its id.
     *
     * @param id the id
     */
    @Override
    public void deleteById(Integer id) {
        iRuleNameRepository.deleteById(id);
    }

    /**
     * Gets a RuleName by id.
     *
     * @param id the RuleName id
     * @return the found RuleName
     */
    @Override
    public RuleName getById(Integer id) {
        return iRuleNameRepository.getById(id);
    }

    /**
     * Updates a RuleName.
     *
     * @param ruleName the RuleName
     * @return the RuleName
     * @throws BadArgument if the RuleName with the given id doesn't exist
     */
    @Override
    public RuleName update(RuleName ruleName) throws BadArgument {
        var existingRuleName = getById(ruleName.getId());
        if (existingRuleName == null) {
            throw new BadArgument("RuleName with id : " + ruleName.getId() + " not found");
        }
        existingRuleName.setName(ruleName.getName());
        existingRuleName.setDescription(ruleName.getDescription());
        existingRuleName.setJson(ruleName.getJson());
        existingRuleName.setSqlPart(ruleName.getSqlPart());
        existingRuleName.setSqlStr(ruleName.getSqlStr());
        existingRuleName.setTemplate(ruleName.getTemplate());
        return iRuleNameRepository.save(ruleName);
    }
}
