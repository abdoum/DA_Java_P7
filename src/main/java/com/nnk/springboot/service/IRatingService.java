package com.nnk.springboot.service;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.exception.BadArgument;

import java.util.List;

public interface IRatingService {

    List<Rating> findAll();

    Rating save(Rating rating);

    void deleteById(Integer id);

    Rating getById(Integer id);

    Rating update(Rating rating) throws BadArgument;

}
