package com.nnk.springboot.service;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    IUserRepository iUserRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Find all user in database.
     *
     * @return the list found users or an empty list if none found
     */
    @Override
    public List<User> findAll() {
        return iUserRepository.findAll();
    }

    /**
     * Saves a user to database.
     *
     * @param user the user to be saved
     * @return the saved user
     */
    @Override
    public User save(User user) {
        var userToPersist = new User();
        userToPersist.setUsername(user.getUsername());
        userToPersist.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userToPersist.setFullname(user.getFullname());
        userToPersist.setRole(user.getRole());
        return iUserRepository.save(userToPersist);
    }

    /**
     * Delete a user by its id.
     *
     * @param id the id
     */
    @Override
    public void deleteById(Integer id) {
        iUserRepository.deleteById(id);
    }

    /**
     * Gets a user by id.
     *
     * @param id the user id
     * @return the found user
     */
    @Override
    public User getById(Integer id) {
        return iUserRepository.getById(id);
    }

    /**
     * Updates a user.
     *
     * @param user the user
     * @return the user
     * @throws BadArgument if the user with the given id doesn't exist
     */
    @Override
    public User update(User user) throws BadArgument {
        var existingUser = getById(user.getId());
        if (existingUser == null) {
            throw new BadArgument("user with id : " + user.getId() + " not found");
        }
        existingUser.setId(user.getId());
        existingUser.setUsername(user.getUsername());
        existingUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        existingUser.setPassword(user.getPassword());
        existingUser.setFullname(user.getFullname());
        existingUser.setRole(user.getRole());
        return iUserRepository.save(existingUser);
    }

    /**
     * Finds a user by id.
     *
     * @param id the id
     * @return optional the found user or null if none found.
     */
    @Override
    public Optional<User> findById(Integer id) {
        return iUserRepository.findById(id);
    }

    /**
     * Deletes a user from database by its id.
     *
     * @param userId the user id
     */
    @Override
    public void delete(Integer userId) {
        iUserRepository.deleteById(userId);
    }
}
