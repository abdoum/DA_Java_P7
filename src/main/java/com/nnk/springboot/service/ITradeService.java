package com.nnk.springboot.service;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.exception.BadArgument;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

public interface ITradeService {
    List<Trade> findAll();

    Trade save(@Valid Trade trade);

    void deleteById(Integer id);

    Trade getById(@Positive Integer id);

    Trade update(Trade trade) throws BadArgument;
}
