package com.nnk.springboot.service;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.exception.BadArgument;

import javax.validation.Valid;
import java.util.List;

public interface IBidListService {

    List<BidList> findAll();

    List<BidList> save(@Valid BidList bidList);

    void deleteById(Integer id);

    BidList getById(Integer id);

    List<BidList> update(BidList bidList) throws BadArgument;
}
