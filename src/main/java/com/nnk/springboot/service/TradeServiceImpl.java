package com.nnk.springboot.service;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.ITradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TradeServiceImpl implements ITradeService {

    @Autowired
    ITradeRepository iTradeRepository;

    /**
     * Find all trade in database.
     *
     * @return the list found trades or an empty list if none found
     */
    @Override
    public List<Trade> findAll() {
        return iTradeRepository.findAll();
    }

    /**
     * Saves a trade to database.
     *
     * @param trade the trade to be saved
     * @return the saved trade
     */
    @Override
    public Trade save(Trade trade) {
        var tradeToPersist = new Trade();
        tradeToPersist.setType(trade.getType());
        tradeToPersist.setAccount(trade.getAccount());
        tradeToPersist.setCreationDate(trade.getCreationDate());
        tradeToPersist.setBenchmark(trade.getBenchmark());
        tradeToPersist.setBook(trade.getBook());
        tradeToPersist.setTrader(trade.getTrader());
        tradeToPersist.setBuyPrice(trade.getBuyPrice());
        tradeToPersist.setBuyQuantity(trade.getBuyQuantity());
        tradeToPersist.setCreationName(trade.getCreationName());
        tradeToPersist.setDealName(trade.getDealName());
        tradeToPersist.setDealType(trade.getDealType());
        tradeToPersist.setRevisionDate(trade.getRevisionDate());
        tradeToPersist.setSecurity(trade.getSecurity());
        tradeToPersist.setSide(trade.getSide());
        tradeToPersist.setSellPrice(trade.getSellPrice());
        tradeToPersist.setSellQuantity(trade.getSellQuantity());
        tradeToPersist.setStatus(trade.getStatus());
        tradeToPersist.setSourceListId(trade.getSourceListId());

        return iTradeRepository.save(tradeToPersist);
    }


    /**
     * Delete a trade by its id.
     *
     * @param id the id
     */
    @Override
    public void deleteById(Integer id) {
        iTradeRepository.deleteById(id);
    }

    /**
     * Gets a trade by id.
     *
     * @param id the trade id
     * @return the found trade
     */
    @Override
    public Trade getById(Integer id) {
        return iTradeRepository.getById(id);
    }

    /**
     * Updates a trade.
     *
     * @param trade the trade
     * @return the trade
     * @throws BadArgument if the trade with the given id doesn't exist
     */
    @Override
    public Trade update(Trade trade) throws BadArgument {
        var existingTrade = getById(trade.getId());
        if (existingTrade == null) {
            throw new BadArgument("trade with id : " + trade.getId() + " not found");
        }
        existingTrade.setType(trade.getType());
        existingTrade.setAccount(trade.getAccount());
        existingTrade.setCreationDate(trade.getCreationDate());
        existingTrade.setBenchmark(trade.getBenchmark());
        existingTrade.setBook(trade.getBook());
        existingTrade.setTrader(trade.getTrader());
        existingTrade.setBuyPrice(trade.getBuyPrice());
        existingTrade.setBuyQuantity(trade.getBuyQuantity());
        existingTrade.setCreationName(trade.getCreationName());
        existingTrade.setDealName(trade.getDealName());
        existingTrade.setDealType(trade.getDealType());
        existingTrade.setRevisionDate(trade.getRevisionDate());
        existingTrade.setSecurity(trade.getSecurity());
        existingTrade.setSide(trade.getSide());
        existingTrade.setSellPrice(trade.getSellPrice());
        existingTrade.setSellQuantity(trade.getSellQuantity());
        existingTrade.setStatus(trade.getStatus());
        existingTrade.setSourceListId(trade.getSourceListId());
        return iTradeRepository.save(existingTrade);
    }
}
