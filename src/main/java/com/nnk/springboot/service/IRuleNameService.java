package com.nnk.springboot.service;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.exception.BadArgument;

import java.util.List;

public interface IRuleNameService {
    List<RuleName> findAll();

    RuleName save(RuleName ruleName);

    void deleteById(Integer id);

    RuleName getById(Integer id);

    RuleName update(RuleName ruleName) throws BadArgument;
}
