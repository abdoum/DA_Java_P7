package com.nnk.springboot.service;


import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.exception.BadArgument;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Positive;
import java.util.List;

public interface ICurvePointService {
    List<CurvePoint> findAll();

    CurvePoint save(CurvePoint curvePoint);

    void deleteById(@NumberFormat(style = NumberFormat.Style.NUMBER) @Positive Integer id);

    CurvePoint getById(@NumberFormat(style = NumberFormat.Style.NUMBER) @Positive Integer id);

    CurvePoint update(CurvePoint curvePoint) throws BadArgument;
}
