package com.nnk.springboot.service;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.repositories.IUserRepository;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Generated
@Service
@Slf4j
public class UserDetailsServiceImpl implements IUserDetailsService {

    @Autowired
    IUserRepository iUserRepository;

    /**
     * Load user by username from database.
     *
     * @param username the username
     * @return the user
     * @throws UsernameNotFoundException if the user is not found
     */
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        User user = iUserRepository.getByUsername(username);
        log.debug(String.valueOf(user));
        if (user == null) {
            throw new UsernameNotFoundException("Could not find user");
        }
        return user;
    }
}
