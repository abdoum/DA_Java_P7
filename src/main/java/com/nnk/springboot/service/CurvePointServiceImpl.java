package com.nnk.springboot.service;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.repositories.ICurvePointRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurvePointServiceImpl implements ICurvePointService {

    @Autowired
    ICurvePointRepository iCurvePointRepository;

    /**
     * Find all curve point in database.
     *
     * @return the list found curve points or an empty list if none found
     */
    @Override
    public List<CurvePoint> findAll() {
        return iCurvePointRepository.findAll();
    }

    /**
     * Saves a curve point to database.
     *
     * @param curvePoint the curve point to be saved
     * @return the saved curve point
     */
    @Override
    public CurvePoint save(CurvePoint curvePoint) {
        var curvePointToPersist = new CurvePoint();
        curvePointToPersist.setCurveId(curvePoint.getCurveId());
        curvePointToPersist.setTerm(curvePoint.getTerm());
        curvePointToPersist.setAsOfDate(curvePoint.getAsOfDate());
        curvePointToPersist.setValue(curvePoint.getValue());
        curvePointToPersist.setCreationDate(curvePoint.getCreationDate());

        return iCurvePointRepository.save(curvePointToPersist);
    }


    /**
     * Delete a curve point by its id.
     *
     * @param id the id
     */
    @Override
    public void deleteById(Integer id) {
        iCurvePointRepository.deleteById(id);
    }

    /**
     * Gets a curve point by id.
     *
     * @param id the curve point id
     * @return the found curve point
     */
    @Override
    public CurvePoint getById(Integer id) {
        return iCurvePointRepository.getById(id);
    }

    /**
     * Updates a curve point.
     *
     * @param curvePoint the curve point
     * @return the curve point
     * @throws BadArgument if the curve point with the given id doesn't exist
     */
    @Override
    public CurvePoint update(CurvePoint curvePoint) throws BadArgument {
        var existingCurvePoint = getById(curvePoint.getId());
        if (existingCurvePoint == null) {
            throw new BadArgument("CurvePoint with id : " + curvePoint.getId() + " not found");
        }
        existingCurvePoint.setCurveId(curvePoint.getCurveId());
        existingCurvePoint.setTerm(curvePoint.getTerm());
        existingCurvePoint.setAsOfDate(curvePoint.getAsOfDate());
        existingCurvePoint.setValue(curvePoint.getValue());
        existingCurvePoint.setCreationDate(curvePoint.getCreationDate());
        return iCurvePointRepository.save(curvePoint);
    }
}
