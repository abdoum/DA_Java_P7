package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.service.IUserService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Generated
@Controller
@Slf4j
public class UserController {

    @Autowired
    private IUserService iUserService;

    /**
     * Displays the Home page.
     *
     * @param model the data model
     * @return the html page
     */
    @RequestMapping("/user/list")
    public String home(Model model) {
        log.info("received a get request to the user/list route");
        List<User> users = iUserService.findAll();
        log.info(users.toString());
        model.addAttribute("users", users);
        return "user/list";
    }

    /**
     * Display the add user form.
     *
     * @param user the user data object
     * @return html page
     */
    @GetMapping("/user/add")
    public String addUser(User user) {
        log.info("received a get request to the user/add route");
        log.info(user.toString());
        return "user/add";
    }

    /**
     * Validate string.
     *
     * @param user   the user
     * @param result the result
     * @param model  the data model
     * @return string   the validation errors, if no errors, it redirects to the users list
     */
    @PostMapping("/user/validate")
    public String validate(@Valid User user, BindingResult result, Model model) {
        log.info(user.toString());
        if (result.hasErrors()) {
            return "user/add";
        }
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(user.getPassword()));
        iUserService.save(user);
        List<User> users = iUserService.findAll();
        model.addAttribute("users", users);
        log.info(user.toString());
        return "redirect:/user/list";
    }

    /**
     * Displays the update form.
     *
     * @param id    the user id
     * @param model the data model
     * @return the html form
     */
    @GetMapping("/user/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        log.info(id.toString());
        User user = iUserService.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        user.setPassword("");
        model.addAttribute("user", user);
        log.info(user.toString());
        return "user/update";
    }

    /**
     * Updates a user.
     *
     * @param id     the user id
     * @param user   the updated user object
     * @param result the validation result
     * @param model  the data model
     * @return the string
     */
    @PostMapping("/user/update/{id}")
    public String updateUser(@PathVariable("id") Integer id, @Valid User user,
                             BindingResult result, Model model) throws BadArgument {
        log.info(user.toString());
        if (result.hasErrors()) {
            return "user/update";
        }
        iUserService.update(user);
        List<User> users = iUserService.findAll();
        model.addAttribute("users", users);
        log.info(users.toString());
        return "redirect:/user/list";
    }

    /**
     * Delete user string.
     *
     * @param id    the id
     * @param model the model
     * @return the string
     */
    @GetMapping("/user/delete/{id}")
    public String deleteUser(@PathVariable("id") Integer id, Model model) {
        iUserService.delete(id);
        model.addAttribute("users", iUserService.findAll());
        return "redirect:/user/list";
    }
}
