package com.nnk.springboot.controllers;

import com.nnk.springboot.repositories.IUserRepository;
import lombok.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Generated
@Controller
@RequestMapping("app")
public class LoginController {

    @Autowired
    private IUserRepository IUserRepository;

    /**
     * Login page.
     *
     * @return the model and view
     */
    @GetMapping("login")
    public ModelAndView login() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("login");
        return mav;
    }

    /**
     * Gets all users.
     *
     * @return the users
     */
    @GetMapping("secure/article-details")
    public ModelAndView getAllUserArticles() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("users", IUserRepository.findAll());
        mav.setViewName("user/list");
        return mav;
    }

    /**
     * Error page.
     *
     * @return the model and view
     */
    @GetMapping("error")
    public ModelAndView error() {
        ModelAndView mav = new ModelAndView();
        String errorMessage = "You are not authorized for the requested data.";
        mav.addObject("errorMsg", errorMessage);
        mav.setViewName("error/403");
        return mav;
    }
}
