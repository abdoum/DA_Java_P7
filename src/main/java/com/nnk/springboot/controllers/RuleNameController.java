package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.service.IRuleNameService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@Generated
@Controller
@Slf4j
public class RuleNameController {

    @Autowired
    IRuleNameService iRuleNameService;

    String ruleNameListAttributeName = "ruleNames";

    /**
     * Displays the RuleName List.
     *
     * @param model the response model
     * @return the html page
     */
    @RequestMapping("/ruleName/list")
    public String home(Model model) {
        log.info("received get request to /ruleName/list");
        var ruleNames = iRuleNameService.findAll();
        model.addAttribute(ruleNameListAttributeName, ruleNames);
        log.info(ruleNames.toString());
        return "ruleName/list";
    }

    /**
     * Displays the Add Curve Point form.
     *
     * @return the form's html markup
     */
    @GetMapping("/ruleName/add")
    public String addBidForm(RuleName ruleName) {
        return "ruleName/add";
    }

    /**
     * Validates the RuleName fields. Redirects to the RuleNames list on success.
     * Displays validations errors otherwise.
     *
     * @param ruleName the Rule
     * @param result   the validation result
     * @param model    the response model
     * @return the html markup
     */
    @PostMapping("/ruleName/validate")
    public String validate(@Valid RuleName ruleName, BindingResult result, Model model) {
        log.info(ruleName.toString());
        if (result.hasErrors()) {
            return "ruleName/add";
        }
        var savedRuleName = iRuleNameService.save(ruleName);
        if (savedRuleName != null) {
            List<RuleName> ruleNameList = iRuleNameService.findAll();
            model.addAttribute(ruleNameListAttributeName, ruleNameList);
            log.info(ruleNameList.toString());
        }
        return "redirect:/ruleName/list";
    }

    /**
     * Show the update RuleName form.
     *
     * @param id    the RuleName id to be retrieved
     * @param model the response model
     * @return the form's html markup
     * @throws BadArgument if no RuleName matching the provided id is found
     */
    @Validated
    @GetMapping("/ruleName/update/{id}")
    public String showUpdateForm(@PathVariable("id") @Positive Integer id, Model model) throws BadArgument {
        log.info(id.toString());
        var ruleName = iRuleNameService.getById(id);
        if (ruleName == null) {
            throw new BadArgument("no RuleName with id " + id + " was found");
        }
        model.addAttribute("ruleName", ruleName);
        log.info(ruleName.toString());
        return "ruleName/update";
    }

    /**
     * Updates a RuleName. Redirects to the RuleNames list on success. Displays validations errors otherwise.
     *
     * @param id       the RuleName id to be retrieved
     * @param ruleName the updated RuleName
     * @param result   the validation result
     * @param model    the response model
     * @return the html markup
     * @throws BadArgument if no RuleName with the given id was found
     */
    @PostMapping("/ruleName/update/{id}")
    public String updateBid(@PathVariable("id") Integer id, @Valid RuleName ruleName,
                            BindingResult result, Model model) throws BadArgument {
        log.info(id.toString());
        var existingRuleName = iRuleNameService.getById(id);
        if (existingRuleName == null) {
            throw new BadArgument("no rule name with id " + id + " was found");
        }
        if (result.hasErrors()) {
            return "ruleName/update";
        }
        var updatedRuleName = iRuleNameService.update(ruleName);
        if (updatedRuleName != null) {
            List<RuleName> ruleNameList = iRuleNameService.findAll();
            model.addAttribute(ruleNameListAttributeName, ruleNameList);
            log.info(ruleNameList.toString());
        }
        return "redirect:/ruleName/list";
    }


    /**
     * Deletes a RuleName. Returns the updated RuleNames list on success.
     *
     * @param id    the RuleName id to be deleted
     * @param model the response model
     * @return the updated RuleNames list
     */
    @GetMapping("/ruleName/delete/{id}")
    public String deleteBid(@PathVariable("id") Integer id, Model model) {
        log.info(id.toString());
        iRuleNameService.deleteById(id);
        List<RuleName> ruleNameList = iRuleNameService.findAll();
        model.addAttribute(ruleNameListAttributeName, ruleNameList);
        log.info(ruleNameList.toString());
        return "redirect:/ruleName/list";
    }
}
