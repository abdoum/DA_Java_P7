package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.service.ITradeService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@Generated
@Controller
@Slf4j
public class TradeController {

    @Autowired
    ITradeService iTradeService;

    String tradeListAttributeName = "trades";

    /**
     * Displays the trade List.
     *
     * @param model the response model
     * @return the html page
     */
    @RequestMapping("/trade/list")
    public String home(Model model) {
        log.info("received get request to /trade/list");
        var trades = iTradeService.findAll();
        model.addAttribute(tradeListAttributeName, trades);
        log.info(trades.toString());
        return "trade/list";
    }

    /**
     * Displays the Add Curve Point form.
     *
     * @return the form's html markup
     */
    @GetMapping("/trade/add")
    public String addBidForm(Trade trade) {
        return "trade/add";
    }

    /**
     * Validates the trade fields. Redirects to the trades list on success.
     * Displays validations errors otherwise.
     *
     * @param result the validation result
     * @param model  the response model
     * @return the html markup
     */
    @PostMapping("/trade/validate")
    public String validate(@Valid Trade trade, BindingResult result, Model model) {
        log.info(trade.toString());
        if (result.hasErrors()) {
            return "trade/add";
        }
        var savedTrade = iTradeService.save(trade);
        if (savedTrade != null) {
            List<Trade> tradeList = iTradeService.findAll();
            model.addAttribute(tradeListAttributeName, tradeList);
            log.info(tradeList.toString());
        }
        return "redirect:/trade/list";
    }

    /**
     * Show the update trade form.
     *
     * @param id    the trade id to be retrieved
     * @param model the response model
     * @return the form's html markup
     * @throws BadArgument if no trade matching the provided id is found
     */
    @GetMapping("/trade/update/{id}")
    public String showUpdateForm(@PathVariable("id") @Positive Integer id, Model model) throws BadArgument {
        log.info(id.toString());
        var trade = iTradeService.getById(id);
        if (trade == null) {
            throw new BadArgument("no trade with id " + id + " was found");
        }
        model.addAttribute("trade", trade);
        log.info(trade.toString());
        return "trade/update";
    }

    /**
     * Updates a trade. Redirects to the trades list on success. Displays validations errors otherwise.
     *
     * @param id     the trade id to be retrieved
     * @param trade  the updated trade
     * @param result the validation result
     * @param model  the response model
     * @return the html markup
     * @throws BadArgument if no trade with the given id was found
     */
    @PostMapping("/trade/update/{id}")
    public String updateBid(@PathVariable("id") Integer id, @Valid Trade trade,
                            BindingResult result, Model model) throws BadArgument {
        log.info(id.toString());
        var existingTrade = iTradeService.getById(id);
        if (existingTrade == null) {
            throw new BadArgument("no trade with id " + id + " was found");
        }
        var updatedTrade = iTradeService.update(trade);
        if (updatedTrade != null) {
            List<Trade> tradeList = iTradeService.findAll();
            model.addAttribute(tradeListAttributeName, tradeList);
            log.info(tradeList.toString());
        }
        return "redirect:/trade/list";
    }

    /**
     * Deletes a trade. Returns the updated trades list on success.
     *
     * @param id    the trade id to be deleted
     * @param model the response model
     * @return the updated trades list
     */
    @GetMapping("/trade/delete/{id}")
    public String deleteBid(@PathVariable("id") Integer id, Model model) {
        log.info(id.toString());
        iTradeService.deleteById(id);
        List<Trade> tradeList = iTradeService.findAll();
        model.addAttribute("trades", tradeList);
        log.info(tradeList.toString());
        return "redirect:/trade/list";
    }
}
