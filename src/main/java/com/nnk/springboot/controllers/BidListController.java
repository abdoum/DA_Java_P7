package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.service.IBidListService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@Generated
@Controller
@Slf4j
public class BidListController {

    @Autowired
    IBidListService iBidListService;

    /**
     * Display the Home page.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping("/bidList/list")
    public String home(Model model) {
        List<BidList> bidList = iBidListService.findAll();
        model.addAttribute("bidList", bidList);
        log.info(bidList.toString());
        return "bidList/list";
    }

    /**
     * Display Add bid form.
     *
     * @param bidList the bid list
     * @return the string
     */
    @GetMapping("/bidList/add")
    public String addBidForm(BidList bidList) {
        return "bidList/add";
    }

    /**
     * Save a bidList. Redirects to list page if successful.
     *
     * @param bidList       the bid list
     * @param bindingResult the binding result
     * @param model         the model
     * @return the list of bids
     */
    @PostMapping("/bidList/validate")
    public String validate(@Valid BidList bidList, BindingResult bindingResult,
                           Model model) {
        log.info(bidList.toString());
        if (bindingResult.hasErrors()) {
            return "bidList/add";
        }

        List<BidList> bidLists = iBidListService.save(bidList);
        log.info(bidLists.toString());
        model.addAttribute("bidList", bidLists);

        return "redirect:/bidList/list";
    }

    /**
     * Show update bid form.
     *
     * @param id    the id
     * @param model the model
     * @return the string
     */
    @Validated
    @GetMapping("/bidList/update/{id}")
    public String showUpdateForm(@PathVariable("id") @Positive Integer id, Model model) {
        log.info(id.toString());
        var bid = iBidListService.getById(id);
        model.addAttribute("bidList", bid);
        log.info(bid.toString());
        return "bidList/update";
    }

    /**
     * Updates a bid and redirects to the bids list page.
     *
     * @param id      the id
     * @param bidList the bid list
     * @param result  the result
     * @param model   the model
     * @return the list of bids string
     */
    @PostMapping("/bidList/update/{id}")
    public String updateBid(@PathVariable("id") @Positive Integer id, @Valid BidList bidList,
                            BindingResult result, Model model) {
        log.info(bidList.toString());
        var bidToBeSaved = new BidList();
        bidToBeSaved.setBidListId(id);
        bidToBeSaved.setAccount(bidList.getAccount());
        bidToBeSaved.setType(bidList.getType());
        bidToBeSaved.setBidQuantity(bidList.getBidQuantity());
        var bidLists = iBidListService.save(bidToBeSaved);
        log.info(bidLists.toString());
        return "redirect:/bidList/list";
    }

    /**
     * Deletes a bid and redirects to the bids list page.
     *
     * @param id    the id
     * @param model the model
     * @return string the list of bids
     */
    @Validated
    @GetMapping("/bidList/delete/{id}")
    public String deleteBid(@PathVariable("id") Integer id, Model model) {
        log.info(id.toString());
        iBidListService.deleteById(id);
        log.info("Deleted bidList with id: " + id.toString());
        return "redirect:/bidList/list";
    }
}
