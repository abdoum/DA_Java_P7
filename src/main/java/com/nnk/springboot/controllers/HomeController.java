package com.nnk.springboot.controllers;

import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Generated
@Controller
@Slf4j
public class HomeController {

    /**
     * Home page.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping("/")
    public String home(Model model) {
        log.info("received a get request to the homepage");
        return "home";
    }

    /**
     * Admin home page.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping("/admin/home")
    public String adminHome(Model model) {
        log.info("received a get request to the ADMIN homepage");
        return "redirect:/bidList/list";
    }


}
