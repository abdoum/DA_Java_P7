package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.service.ICurvePointService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@Generated
@Controller
@Slf4j
public class CurveController {

    @Autowired
    ICurvePointService iCurvePointService;

    String curvePointListAttributeName = "curvePoints";

    /**
     * Displays the Curve Points List Table.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping("/curvePoint/list")
    public String home(Model model) {
        log.info("received get request to /curvePoint/list");
        var curvePoints = iCurvePointService.findAll();
        model.addAttribute(curvePointListAttributeName, curvePoints);
        log.info(curvePoints.toString());
        return "curvePoint/list";
    }

    /**
     * Displays the Add Curve Point form.
     *
     * @return the form's html markup
     */
    @GetMapping("/curvePoint/add")
    public String addBidForm(CurvePoint curvePoint) {
        return "curvePoint/add";
    }

    /**
     * Validates the curve point fields. Redirects to the curve points list on success.
     * Displays validations errors otherwise.
     *
     * @param curvePoint the curve point
     * @param result     the validation result
     * @param model      the response model
     * @return the html markup
     */
    @PostMapping("/curvePoint/validate")
    public String validate(@Valid CurvePoint curvePoint, BindingResult result, Model model) {
        log.info(curvePoint.toString());
        if (result.hasErrors()) {
            return "curvePoint/add";
        }
        var savedCurvePoint = iCurvePointService.save(curvePoint);
        if (savedCurvePoint != null) {
            List<CurvePoint> curvePointList = iCurvePointService.findAll();
            model.addAttribute(curvePointListAttributeName, curvePointList);
            log.info(curvePointList.toString());
        }
        return "redirect:/curvePoint/list";
    }

    /**
     * Show the update curve point form.
     *
     * @param id    the curve point id to be retrieved
     * @param model the response model
     * @return the form's html markup
     * @throws BadArgument if no curve point matching the provided id is found
     */
    @Validated
    @GetMapping("/curvePoint/update/{id}")
    public String showUpdateForm(@PathVariable("id") @Positive Integer id, Model model) throws BadArgument {
        log.info(id.toString());
        var curvePoint = iCurvePointService.getById(id);
        if (curvePoint == null) {
            throw new BadArgument("no curve point with id " + id + " was found");
        }
        model.addAttribute("curvePoint", curvePoint);
        log.info(curvePoint.toString());
        return "curvePoint/update";
    }

    /**
     * Updates a curve point. Redirects to the curve points list on success. Displays validations errors otherwise.
     *
     * @param id         the curve point id to be retrieved
     * @param curvePoint the updated curve point
     * @param result     the validation result
     * @param model      the response model
     * @return the html markup
     * @throws BadArgument the bad argument
     */
    @PostMapping("/curvePoint/update/{id}")
    public String updateBid(@PathVariable("id") Integer id, @Valid CurvePoint curvePoint,
                            BindingResult result, Model model) throws BadArgument {
        log.info(id.toString());
        var existingCurvePoint = iCurvePointService.getById(id);
        if (existingCurvePoint == null) {
            throw new BadArgument("no curve point with id " + id + " was found");
        }
        var updatedCurvePoint = iCurvePointService.update(curvePoint);
        if (updatedCurvePoint != null) {
            List<CurvePoint> curvePointList = iCurvePointService.findAll();
            model.addAttribute(curvePointListAttributeName, curvePointList);
            log.info(curvePointList.toString());
        }
        return "redirect:/curvePoint/list";
    }


    /**
     * Deletes a curve point. Returns the updated curve points list on success.
     *
     * @param id    the curve point id to be deleted
     * @param model the response model
     * @return the updated curve points list
     */
    @GetMapping("/curvePoint/delete/{id}")
    public String deleteBid(@PathVariable("id") Integer id, Model model) {
        log.info(id.toString());
        iCurvePointService.deleteById(id);
        List<CurvePoint> curvePointList = iCurvePointService.findAll();
        model.addAttribute("curvePoints", curvePointList);
        log.info(curvePointList.toString());
        return "redirect:/curvePoint/list";
    }
}
