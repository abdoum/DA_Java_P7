package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.exception.BadArgument;
import com.nnk.springboot.service.IRatingService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@Generated
@Controller
@Slf4j
public class RatingController {

    @Autowired
    IRatingService iRatingService;

    String ratingListAttributeName = "ratings";

    /**
     * Displays the rating List.
     *
     * @param model the response model
     * @return the html page
     */
    @RequestMapping("/rating/list")
    public String home(Model model) {
        log.info("received get request to /rating/list");
        var ratings = iRatingService.findAll();
        model.addAttribute(ratingListAttributeName, ratings);
        log.info(ratings.toString());
        return "rating/list";
    }

    /**
     * Displays the Add Curve Point form.
     *
     * @return the form's html markup
     */
    @GetMapping("/rating/add")
    public String addBidForm(Rating rating) {
        return "rating/add";
    }

    /**
     * Validates the rating fields. Redirects to the ratings list on success.
     * Displays validations errors otherwise.
     *
     * @param result the validation result
     * @param model  the response model
     * @return the html markup
     */
    @PostMapping("/rating/validate")
    public String validate(@Valid Rating rating, BindingResult result, Model model) {
        log.info(rating.toString());
        if (result.hasErrors()) {
            return "rating/add";
        }
        var savedRating = iRatingService.save(rating);
        if (savedRating != null) {
            List<Rating> ratingList = iRatingService.findAll();
            model.addAttribute(ratingListAttributeName, ratingList);
            log.info(ratingList.toString());
        }
        return "redirect:/rating/list";
    }

    /**
     * Show the update rating form.
     *
     * @param id    the rating id to be retrieved
     * @param model the response model
     * @return the form's html markup
     * @throws BadArgument if no rating matching the provided id is found
     */
    @Validated
    @GetMapping("/rating/update/{id}")
    public String showUpdateForm(@PathVariable("id") @Positive Integer id, Model model) throws BadArgument {
        log.info(id.toString());
        var rating = iRatingService.getById(id);
        if (rating == null) {
            throw new BadArgument("no rating with id " + id + " was found");
        }
        model.addAttribute("rating", rating);
        log.info(rating.toString());
        return "rating/update";
    }

    /**
     * Updates a rating. Redirects to the ratings list on success. Displays validations errors otherwise.
     *
     * @param id     the rating id to be retrieved
     * @param Rating the updated rating
     * @param result the validation result
     * @param model  the response model
     * @return the html markup
     * @throws BadArgument if no rating with the given id was found
     */
    @PostMapping("/rating/update/{id}")
    public String updateBid(@PathVariable("id") Integer id, @Valid Rating rating,
                            BindingResult result, Model model) throws BadArgument {
        log.info(id.toString());
        var existingRating = iRatingService.getById(id);
        if (existingRating == null) {
            throw new BadArgument("no rating with id " + id + " was found");
        }
        var updatedRating = iRatingService.update(rating);
        if (updatedRating != null) {
            List<Rating> ratingList = iRatingService.findAll();
            model.addAttribute(ratingListAttributeName, ratingList);
            log.info(ratingList.toString());
        }
        return "redirect:/rating/list";
    }

    /**
     * Deletes a rating. Returns the updated ratings list on success.
     *
     * @param id    the rating id to be deleted
     * @param model the response model
     * @return the updated ratings list
     */
    @GetMapping("/rating/delete/{id}")
    public String deleteBid(@PathVariable("id") Integer id, Model model) {
        log.info(id.toString());
        iRatingService.deleteById(id);
        List<Rating> ratingList = iRatingService.findAll();
        model.addAttribute("ratings", ratingList);
        log.info(ratingList.toString());
        return "redirect:/rating/list";
    }
}
