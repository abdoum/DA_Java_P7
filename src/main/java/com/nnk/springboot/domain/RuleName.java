package com.nnk.springboot.domain;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@Generated
@Getter
@Setter
@ToString
@DynamicUpdate
@Entity
@Table(name = "rulename")
public class RuleName {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Pattern(regexp = "[\\p{Alnum}\\h]{0,50}")
    private String name;

    @Pattern(regexp = "[\\p{Alnum}\\h]{0,50}")
    private String description;

    @Pattern(regexp = "[\\p{Alnum}\\h {}:,\\n\"]*", message = "must be valid json")
    private String json;

    @Pattern(regexp = "[\\p{Alnum}\\h\\p{Punct}]{0,500}", flags = Pattern.Flag.MULTILINE)
    private String template;

    @Pattern(regexp = "[\\w\\h\\`\\;()%\\*]{0,500}", flags = Pattern.Flag.MULTILINE, message = "must be valid sql")
    @Column(name = "sqlstr")
    private String sqlStr;

    @Pattern(regexp = "[\\w\\h\\`()\\;%\\*]{0,500}", flags = Pattern.Flag.MULTILINE, message = "must be valid sql")
    @Column(name = "sqlpart")
    private String sqlPart;

    public RuleName(String name, String description, String json, String template, String sqlStr, String sqlPart) {
    }

    public RuleName() {

    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        RuleName ruleName = (RuleName) o;
        return Objects.equals(id, ruleName.id);
    }
}