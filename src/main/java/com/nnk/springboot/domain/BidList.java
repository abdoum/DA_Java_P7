package com.nnk.springboot.domain;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;

@Generated
@Getter
@Setter
@ToString
@Entity
@Table(name = "bidlist")
public class BidList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bidlistid", nullable = false)
    private Integer bidListId;

    @NotBlank(message = "Account is mandatory")
    private String account;

    @NotBlank(message = "Type is mandatory")
    @Size(min = 2, max = 50)
    private String type;

    @Positive(message = "Bid quantity must be greater than 0")
    @Digits(integer = 5, fraction = 0)
    @Column(name = "bidquantity")
    private Double bidQuantity;

    @Positive(message = "Ask quantity must be greater than 0")
    @Column(name = "askquantity")
    @Digits(integer = 5, fraction = 0)
    private Double askQuantity;

    @Positive(message = "Bid must be greater than 0")
    @Digits(integer = 5, fraction = 2)
    private Double bid;

    @Positive(message = "Ask must be greater than 0")
    @Digits(integer = 5, fraction = 0)
    private Double ask;

    @Pattern(regexp = "\\p{Alnum}")
    private String benchmark;

    @FutureOrPresent
    @Column(name = "bidlistdate")
    private Timestamp bidListDate;

    @Pattern(regexp = "[\\p{Alnum}\\h\\p{Punct}]{0,500}", flags = Pattern.Flag.MULTILINE)
    private String commentary;
    private String security;
    private String status;
    private String trader;
    private String book;

    @Column(name = "creationname")
    private String creationName;

    @PastOrPresent
    @Column(name = "creationdate")
    private Timestamp creationDate;

    @Column(name = "revisionname")
    private String revisionName;

    @Column(name = "revisiondate")
    private Timestamp revisionDate;

    @Column(name = "dealname")
    private String dealName;

    @Column(name = "dealtype")
    private String dealType;


    @Column(name = "sourcelistid")
    private String sourceListId;

    private String side;

    public BidList(String account, String type, double bid) {
    }

    public BidList() {

    }

}