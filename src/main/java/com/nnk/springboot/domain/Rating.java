package com.nnk.springboot.domain;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Positive;

@Generated
@Getter
@Setter
@ToString
@DynamicUpdate
@Entity
@Table(name = "rating")
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "moodysrating")
    private String moodysRating;

    @Column(name = "sandprating")
    private String sandPRating;

    @Column(name = "fitchrating")
    private String fitchRating;

    @Positive
    @Digits(integer = 5, fraction = 0)
    @Column(name = "ordernumber")
    private Integer orderNumber;

    public Rating(String moodysRating, String sandPRating, String fitchRating, Integer orderNumber) {
        this.moodysRating = moodysRating;
        this.sandPRating = sandPRating;
        this.fitchRating = fitchRating;
        this.orderNumber = orderNumber;
    }

    public Rating() {
    }
}