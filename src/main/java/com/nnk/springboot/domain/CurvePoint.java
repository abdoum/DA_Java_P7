package com.nnk.springboot.domain;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;

@Generated
@Getter
@Setter
@ToString
@DynamicUpdate
@Entity
@Table(name = "curvepoint")
public class CurvePoint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Positive
    @NotNull
    @Column(name = "curveid")
    private Integer curveId;

    @Future
    @Column(name = "asofdate")
    private Timestamp asOfDate;

    @PositiveOrZero
    @Digits(integer = 5, fraction = 2)
    private Double term;

    @PositiveOrZero
    @Digits(integer = 5, fraction = 2)
    private Double value;

    @PastOrPresent
    @Column(name = "creationdate")
    private Timestamp creationDate;

    public CurvePoint(int id, double term, double value) {
    }

    public CurvePoint() {

    }

    public void setAsOfDate(Timestamp asOfDate) {
        this.asOfDate = Optional.ofNullable(asOfDate).orElse(null);
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate != null ? asOfDate : Timestamp.from(Instant.now());
    }
}