package com.nnk.springboot.domain;

import com.nnk.springboot.annotation.SellPriceGreaterThanBuyPrice;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;

@Generated
@Getter
@Setter
@ToString
@DynamicUpdate
@Entity
@Table(name = "trade")
@SellPriceGreaterThanBuyPrice(message = "Sell price must be greater than buy price")
public class Trade {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "tradeid", nullable = false)
    private Integer id;

    @NotBlank(message = "account is required")
    @Length(min = 1, max = 30)
    private String account;

    @NotBlank(message = "type is required")
    @Length(min = 1, max = 30, message = "Type must be between 1 and 30 characters long")
    private String type;

    @Digits(integer = 5, fraction = 0)
    @Column(name = "buyquantity")
    private Double buyQuantity;

    @Digits(integer = 5, fraction = 0)
    @Column(name = "sellquantity")
    private Double sellQuantity;

    @Digits(integer = 5, fraction = 2)
    @Column(name = "buyprice")
    private Double buyPrice;

    @Digits(integer = 5, fraction = 2)
    @Column(name = "sellprice")
    private Double sellPrice;

    @Pattern(regexp = "[\\w\\h]{0,100}")
    private String benchmark;

    @PastOrPresent
    @Column(name = "tradedate")
    private Timestamp tradeDate;

    @Pattern(regexp = "[\\w\\h]{0,100}")
    private String security;

    @Pattern(regexp = "[\\w\\h]{0,10}")
    private String status;

    @Pattern(regexp = "[\\w\\h]{0,100}")
    private String trader;

    @Pattern(regexp = "[\\w\\h]{0,100}")
    private String book;

    @Pattern(regexp = "[\\w\\h]{0,100}")
    @Column(name = "creationname")
    private String creationName;

    @PastOrPresent
    @Column(name = "creationdate")
    private Timestamp creationDate;

    @Pattern(regexp = "[\\w\\h]{0,100}")
    @Column(name = "revisionname")
    private String revisionName;

    @Future
    @Column(name = "revisiondate")
    private Timestamp revisionDate;

    @Pattern(regexp = "[\\w\\h]{0,100}")
    @Column(name = "dealname")
    private String dealName;

    @Pattern(regexp = "[\\w\\h]{0,100}")
    @Column(name = "dealtype")
    private String dealType;

    @Pattern(regexp = "[\\w\\h]{0,100}")
    @Column(name = "sourcelistid")
    private String sourceListId;

    @Pattern(regexp = "[\\w\\h]{0,100}")
    private String side;

    public Trade(String account, String type) {
    }

    public Trade() {

    }
}