![](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white)
![](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
![](/gitlab/last-commit/gitlab-org/gitlab-development-kit)
![](/maven/v/metadata-url/repo1.maven.org/maven2/com/google/code/gson/gson/maven-metadata.xml)
![](/gitlab/coverage/:abdoum/:DA_Java_P7/:develop)

# Poseidon :ocean:

![](/Users/dr/Documents/Formation/openclassrooms/projets/P7_abdallah_mansour/src/main/resources/static/logo.png)

[[_TOC_]]

## 1. Technologies:

1. Framework: Spring Boot v2.0.4
2. Java 8
3. Thymeleaf
4. Bootstrap v.4.3.1

## 2. Main features:

- Authentication using spring security cookies
- A rest api for data fetching
- Thymeleaf for html template rendering
- MVC based architecture
- **Javadoc** & **jacoco test coverage reports** are available in the `/target` directory
- Custom 403 and 404 error pages

## 3. Run the project on your machine

### Requirements

- [Mysql ^8.0.0]((https://dev.mysql.com/downloads/))
- [JDK 11](https://adoptopenjdk.net/)
- [Maven ~4.0.0](https://maven.apache.org/install.html)

### Install mysql server:

[Select the proper installer for your operating system](https://dev.mysql.com/downloads/)

Launch the installer and hit next until it finishes installing (make sure to check `run mysql server` before quitting
the installer.

You may also install mysql shell and add it to your pass if you don't have a mysql gui client

### Clone this repository

```shell
git clone https://gitlab.com/abdoum/DA_Java_P7.git
```

### Create the schema and populate it with some data

```shell
cd src/main/resources/data.sql
mysql -u {replace_by_your_mysql_username} -p < data.sql
```

### Setup your database credentials

Add your credentials:

```properties
# replace the following inside src/main/resources/application.properties
spring.datasource.url=DEC(yourDatabaseUrl)
spring.datasource.username=DEC(yourUsername)
spring.datasource.password=DEC(yourPassword)
```

### Encrypt your credentials

- Run the following command in your terminal
  _(make sure you are in the root directory of the project)_

```shell
mvn jasypt:encrypt -Djasypt.encryptor.password=<replace this with your custom password>
```

- This command should result in something similar to the following being generated in your 'application.properties' file

```properties
spring.datasource.url=ENC(raqbE8JG/dQd+kW66/FWVnPsvkmyJDr9lOepZU0GSuCTY9QfGf8kioIgQeiGtSVoUOEVoa++qFKr7L7/ldMPbMg)
spring.datasource.username=ENC(REFEFreferFRERGefhJIyjuyjkujtryhrt+2xKLGfHfQ7DLDK)
spring.datasource.password=ENC(AW/zZhWHhOH36D4BIdm3dn+GTRgrtevdegtqegjiktrojERGtyjyikgYT)
```

### Run the application

- From the command line, and the encrypted properties should be decrypted at runtime

```shell
mvn spring-boot:run -Djasypt.encryptor.password=<replace this with the previous password used for encryption>
```

- Open your browser and go to : [http://localhost:8080/](http://localhost:8080/)

### Run tests

```bash
mvn test
```

![](src/main/resources/static/tests_run.gif)

### Data structure

![](src/main/resources/static/data_structure.png)

### Class Diagram

![](src/main/resources/static/uml_diagram.png)
